const express = require('express');
const app = express();

require('./users/user.routes')(app);

module.exports = app;