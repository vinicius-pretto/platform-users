const { users } = require('./data.json');

module.exports = (app) => {
    app.get('/users', (req, res, next) => {
        res.json({ users });
    });
}